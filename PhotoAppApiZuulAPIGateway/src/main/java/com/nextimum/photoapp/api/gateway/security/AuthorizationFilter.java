package com.nextimum.photoapp.api.gateway.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.Jwts;

public class AuthorizationFilter extends BasicAuthenticationFilter {

	private Environment environment;

	public AuthorizationFilter(AuthenticationManager authManager, Environment env) {
		super(authManager);
		this.environment = env;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		String autorizationHeader = req.getHeader(environment.getProperty("autorization.token.header.name"));

		if (autorizationHeader == null
				|| !autorizationHeader.startsWith(environment.getProperty("autorization.token.header.prefix"))) {
			chain.doFilter(req, res);
			return;
		}

		UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

		SecurityContextHolder.getContext().setAuthentication(authentication);

	}

	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest req) {
		String autorizationHeader = req.getHeader(environment.getProperty("autorization.token.header.name"));
		if (autorizationHeader == null) {
			return null;
		}
		
		String token = autorizationHeader.replace(environment.getProperty("autorization.token.header.prefix"), "");
		
		String userId = Jwts.parser()
				.setSigningKey(environment.getProperty("token.secret"))
				.parseClaimsJws(token)
				.getBody()
				.getSubject();
		
		if (userId == null) {
			return null;
		}
		
		return new UsernamePasswordAuthenticationToken(userId, null,new ArrayList<>());
		
		
	}

}
